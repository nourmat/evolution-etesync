/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* e-etesync-defines.h
 *
 * SPDX-FileCopyrightText: (C) 2020 Nour E-Din El-Nhass <nouredinosama.gmail.com>
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#ifndef E_ETESYNC_DEFINES_H
#define E_ETESYNC_DEFINES_H

#define E_ETESYNC_CREDENTIAL_ENCRYPTION_PASSWORD "encryption_password"
#define E_ETESYNC_CREDENTIAL_TOKEN "token"
#define E_ETESYNC_CREDENTIAL_DERIVED_KEY "derived_key"

#endif /* E_ETESYNC_DEFINES_H */
